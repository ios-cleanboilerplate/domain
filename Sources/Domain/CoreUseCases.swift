//
//  File.swift
//  
//
//  Created by Behrad Kazemi on 6/5/22.
//

import Foundation
public protocol CoreUseCase {
  
  func getExampleUseCase() -> ExampleUseCaseInterface
  
}
