//
//  EntityModel.swift
//  
//
//  Created by Behrad Kazemi on 6/5/22.
//

import Foundation

public struct EntityModel {
  
  public let fieldName: String
  
  public init(fieldName: String) {
    self.fieldName = fieldName
  }
}
